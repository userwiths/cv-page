const work_history = [
  {
  "company": "Programmista;",
  "position": "Summer Internship",
  "start_date": "01.06.2018",
  "end_date": "01.09.2018",
  "icon":"./Images/programista.png"
}, {
  "company": "SBTech",
  "position": "Programming Academy",
  "start_date": "",
  "end_date": "",
  "icon":"./Images/sbtech.png"
}, {
  "company": "BiSoft",
  "position": "Software developer",
  "start_date": "01.12.2020",
  "end_date": "01.06.2021",
  "icon":"./Images/bisoft.jpg"
}, {
  "company": "Beluga IT",
  "position": "Software developer",
  "start_date": "10.08.2021",
  "end_date": "",
  "icon":"./Images/beluga.png"
}];
const languages = [
  {
  "name": "C#",
  "description": "<p>Known as C# for short and C-Sharp otherwise.</p>\
    <p>A language tightly incorporated into the microsoft working enviroment.</p>\
    <p>Despite being known as a Microsoft technology C# has a so called 'Core' version, that is meant to be able to run on any platform.</p>"
}, {
  "name": "PHP",
  "description": "<p>A language mainly known for web development.</p>\
    <p>Services of the sort of Magento & Wordpress are based on it.</p>\
    <p>Pretty well known for its commercial uses as a template for more complex sites.</p>"
}, {
  "name": "Python",
  "description": "<p>A versatile scripting language that can be used in many different areas</p>\
  <p>My personal experience with it is related to its 2 famous web technologies Flask & Django used to develop web sites.</p>\
  <p>As well as with Turtle(used for drawign graphics),Numpy(used for machine-learning) & TKinter (GUI for python).</p>"
}, {
  "name": "Web Technologies",
  "description": "<p>Web Technologies refers to a set of languages and markups that are an integral part of every web page or site.</p>\
    <ul>\
        <li>HTML - Basic layout and order of a web page.</li>\
        <li>CSS - Styling and making the web page responsive</li>\
        <li>JavaScript - Programming certain logic for the web page.</li>\
    </ul>"
}, {
  "name": "Go",
  "description": "<p>A language mainly known for web development and the fact that it is created and supported by the IT giant Google.</p>"
}, {
  "name": "C & C++",
  "description": "<p>Relatively simple language, know for its scientific usage and mathematical application.\
    Despite its scientific orientation, it has a wide variety of tools allowing development in many areas.</p>"
}];
const datastores=[
  {
    "name":"SQL",
    "description":"<p>SQL is a relational database. It is based around tables.</p>\
    <p>Each table has columns and rows and is able to save a certain model(scheme) of data in it.</p>\
    <p>Tables can be mapped together to create complex data structures.</p>\
    <ul class='secret-list'>\
        <li>MSSQL - Microsoft's SQL Server.</li>\
        <li>MySql - Cross platform SQL Server.</li>\
        <li>SqlLite - A small implementation of an SQL server, mostly used in Android.</li>\
    </ul>"
  },{
    "name":"No-Sql",
    "description":"<p>No-SQL databases are those which contain the information in so called 'documents'.</p>\
    <p>They do not rely on tables and are much more flexible in their use.</p>\
    <p>In practice, I have worked extensively with MongoDB, in theory with many more of the 'Document' base databases.</p>"
  },{
    "name":"BoltDB",
    "description":"<p>A key-value database table.</p>\
    <p>Mainly integrated with the Go language.</p>\
    <p>Provides simple and straight forward ORM, supports transactions, and allows the saving of information in a file.</p>"
  }
];
const frameworks=[
  {
    "name":"Entity Framework",
    "description":"<p>A framework implementing a fliud ORM which allows easily managing complex programming objects as entities in a database.</p>\
    <p>Entity Framework is used in the Microsoft enviroment mostly with C# in order to ease the management of the database.</p>"
  },{
    "name":"React",
    "description":"<p>React is a frontend framework used to develop component based applications.</p>\
    <p>It uses TypeScript which allows writing a single piece of code which is then able to be compiled into different versions of its coresponding javascript.</p>"
  },{
    "name":"Magento 2",
    "description":"<p>A framework focused towards e-commerce & online shoping. Build upon PHP and basically reeady to use from scratch.</p>\
    <p>Has a warge community contributing with different addons and allowing many customizable changes.</p>"
  },{
    "name":"Laravel",
    "description":"<p>A PHP framework with a wide variety of application.</p>\
    <p>Used for creation of websites & online platforms.</p>"
  },{
    "name":"Vue",
    "description":"<p>JavaScript based front-end framework, used in creation of modular websites.</p>"
  },{
    "name":"RequireJS",
    "description":"RequireJS is a framework meant to make the use of javascript modules easily."
  }
];
const version_control=[
  { "name":"Git"},
  { "name":"SVN"},
  { "name":"Microsoft Source Safe"}
];
const other=[
  { "name":"NPM"},
  { "name":"Composer"},
  { "name":"Docker"},
  { "name":"LXD/LXC"},
  { "name":"Nginx/ISS/Apache"}
];
const relatedWorks=[
  {
    "name":"Yota",
    "url":"https://yota.bg",
    "category":"other"
  },{
    "name":"Hermes Books",
    "url":"https://hermesbooks.bg",
    "category":"bookstore"
  },{
    "name":"Bookspace",
    "url":"https://bookspace.bg",
    "category":"bookstore"
  },{
    "name":"Office Market",
    "url":"https://officemarket.bg",
    "category":"other"
  },{
    "name":"Wedo Care",
    "url":"https://wedo-care.com",
    "category":"other"
  },{
    "name":"Agrina",
    "url":"https://agrina.com",
    "category":"other"
  },{
    "name":"TNC",
    "url":"https://tnc.com",
    "category":"other"
  }
];