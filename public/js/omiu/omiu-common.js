/**
 * @omiu/common v0.0.12 http://omijs.org
 * Front End Cross-Frameworks Framework.
 * By dntzhang https://github.com/dntzhang
 * Github: https://github.com/Tencent/omi
 * MIT Licensed.
 */

(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@ctrl/tinycolor')) :
    typeof define === 'function' && define.amd ? define(['exports', '@ctrl/tinycolor'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global['@omiu/common'] = {}, global.tinycolor));
}(this, (function (exports, tinycolor) { 'use strict';

    theme();
    document.addEventListener('DOMContentLoaded', function () {
        theme();
    });
    var cssVarMap = {
        primary: '#07c160',
        danger: '#fa5151',
        surface: '#ffffff',
        'on-primary': '#ffffff',
        'on-danger': '#ffffff',
        'on-surface': '#000000',
        background: '#ffffff',
        'small-radius': '4px',
        'medium-radius': '4px',
        'large-radius': '0px',
        'font-family': '-apple-system-font,"Helvetica Neue",sans-serif'
    };
    function theme() {
        if (document.body && !document.body.style.getPropertyValue('--o-primary')) {
            for (var key in cssVarMap) {
                setCssVar(key, cssVarMap[key]);
            }
            setTheme('primary', cssVarMap['primary']);
            setTheme('danger', cssVarMap['danger']);
        }
    }
    function getFadedColor(color, fade) {
        var alpha = color.getAlpha();
        color.setAlpha(alpha * (1 - fade));
        return color.toString();
    }
    function setCssVar(key, value) {
        var style = document.body.style;
        style.setProperty('--o-' + key, value);
    }
    /**
     * 设置主题色相关
     * @param key
     * @param value
     */
    function setTheme(key, value) {
        var style = document.body.style;
        var fadeMap = {
            little: 0.382,
            some: 0.618,
            more: 0.759,
            lot: 0.9
        };
        var color = new tinycolor.TinyColor(value);
        for (var type in fadeMap) {
            style.setProperty("--o-" + key + "-fade-" + type, getFadedColor(color, fadeMap[type]));
        }
        style.setProperty("--o-" + key + "-active", color.darken(10).toString());
    }
    /**
     * 设置主色调
     * @param color
     */
    function setThemePrimary(color) {
        setTheme('primary', color);
    }
    var index = {
        setTheme: setTheme,
        setThemePrimary: setThemePrimary
    };
    if (typeof window !== undefined) {
        //@ts-ignore
        window.Omiu = {
            setTheme: setTheme,
            setThemePrimary: setThemePrimary
        };
    }

    exports.default = index;
    exports.setTheme = setTheme;
    exports.setThemePrimary = setThemePrimary;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=index.js.map
