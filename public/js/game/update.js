function update(){
    cursors = this.input.keyboard.createCursorKeys();
    let moving=false;
    let on_ground=player.body.onFloor();
   
    moving = moving=cursors.right.isDown || cursors.left.isDown || cursors.up.isDown;
    if(on_ground){
        if (cursors.left.isDown)
        {
            player.setVelocityX(-1*playerStats.speed);
            player.anims.play('run', true);
        }
        else if (cursors.right.isDown)
        {
            player.setVelocityX(playerStats.speed);
            player.anims.play('run', true);
        }

        if(!moving)
        {
            player.setVelocityX(0);

            if (keyA.isDown)
            {
                //Three arguments are needed, else it does not play all frames.
                player.anims.play("att1",10,false);
                return;
            }else if(keyS.isDown)
            {
                //Three arguments are needed, else it does not play all frames.
                player.anims.play("att2",10,false);
                return;
            }else{
                player.anims.play("idle");
            }
        }

        if (cursors.up.isDown)
        {
            player.setVelocityY(-1*gravity*playerStats.weight);
            player.anims.play("fall");
            return;
        }
    }
    if (!on_ground)
    {
        player.anims.play("fall");
    }
    if (cursors.left.isDown)
    {
        player.setFlipX(1);
    }else if (cursors.right.isDown){
        player.setFlipX(0);
    }
    this.physics.world.collide(player, worldItems);
}