function create(){
    keyS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    
    player = this.physics.add.sprite(100, 450, 'attack1');
    player.setBounce(0.0);
    player.setCollideWorldBounds(true);
    player.body.setGravityY(300);
    this.anims.create({
        key: 'idle',
        frames: this.anims.generateFrameNames('m', { start: 0, end: 4 }),
        frameRate: 10,
        repeat: -1
    });

    this.anims.create({
        key: 'run',
        frames: this.anims.generateFrameNames('m', { start: 5, end: 11 }),
        frameRate: 20
    });

    this.anims.create({
        key: 'fall',
        frames: this.anims.generateFrameNames('m', { start: 12, end: 15 }),
        frameRate: 10,
        repeat: -1
    });
    
    this.anims.create({
        key: 'att1',
        frames: this.anims.generateFrameNames('attack1', { start: 0, end: 4 }),
        frameRate:10
    });
    this.anims.create({
        key: 'att2',
        frames: this.anims.generateFrameNames('attack2', { start: 0, end: 4 }),
        frameRate:10
    });
    this.add.text(400,300,playerStats.name).setOrigin(0.5);
    
    //this.physics.add.staticImage(500, 300, 'bar');
    //worldItems=[mgraphic];
}

function populateWithItems(){}

function createUi(){}