function preload(){
    this.load.spritesheet('m', 
        '/cv-page/assets/sprite_full.png',
        { frameWidth: 200, frameHeight: 200 }
    );
    this.load.spritesheet('attack1', 
        '/cv-page/assets/main_attack_1.png',
        { frameWidth: 200, frameHeight: 200 }
    );
    this.load.spritesheet('attack2', 
        '/cv-page/assets/main_attack_2.png',
        { frameWidth: 200, frameHeight: 200 }
    );
}