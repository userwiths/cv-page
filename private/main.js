"use strict";

var body = document.querySelector('body');
/**
 * Sets theme to DARK.
 */

function darkTheme() {
  body.style.setProperty('--main-color', 'black');
  body.style.setProperty('--text-color', 'white');
}
/**
* Sets theme to WHITE
*/


function whiteTheme() {
  body.style.setProperty('--main-color', 'white');
  body.style.setProperty('--text-color', 'black');
}
/**
 * Adds two numbers together.
 * @param {string} text Text to display in the center of the arrow.
 * @param {string} stroke Color of the stroke either rgba() or name.
 * @param {string} fill Color of the fill either rgba() or name.
 * @param {int} width Width of the arrow
 * @return {string}
 */


function createSvgArrow(text, stroke, fill, width) {
  stroke = 'rgba(0,0,0,1)';
  fill = 'rgba(106,82,62,1)';
  var innerHtml = '<svg> <path fill="' + fill + '" stroke="' + stroke + '"';
  innerHtml += 'd="M 0,15  l ' + width + ',0   l 15,15';
  innerHtml += ' l -15,15    l -' + width + ',0  z" />';
  innerHtml += '<text x="5" y="35" class="title-text">' + text + '</text> </svg>';
  return innerHtml;
}
/**
 * @param {string} selector CSS selector to which to apply SVG arrow.
 * @return {void}
 */


function createAllArrows(selector) {
  var items = document.querySelectorAll(selector);

  for (var i = 0; i < items.length; i++) {
    items[i].innerHTML = createSvgArrow(items[i].innerHTML, '', '', items[i].clientWidth);
  }
}
/**
 * @param {string} itemSelector CSS selector for the managed items.
 */


function manageItemFocus(itemSelector) {
  var items = document.querySelectorAll(itemSelector); // const focused=document.querySelectorAll(itemSelector+' .has-focus');

  for (var i = 0; i < items.length; i++) {
    items[i].addEventListener('hover', function (event) {
      event.currentTarget.classList.add('has-focus');
    });
  }
}
/**
 * Executes on a given interval in order to rotate the carousel.
 */


function carouselTick() {
  var items = document.querySelectorAll('.carousel-item');
  var hiding;
  var appearing;
  var next;
  var prepare;

  for (var i = 0; i < items.length; i++) {
    if (items[i].classList.contains('appear')) {
      appearing = i;
      next = (i + 1) % items.length;
    } else if (items[i].classList.contains('hide')) {
      hiding = i;
    } else if (items[i].classList.contains('prepare')) {
      items[i].classList.remove('prepare');
      prepare = (i + 1) % items.length;
    }
  }

  items[hiding].classList.remove('hide');
  items[appearing].classList.remove('appear');
  items[appearing].classList.add('hide');
  items[next].classList.add('appear');
  items[prepare].classList.add('prepare');
}
/**
 * Initialize the web page.
 */


function init() {
  // Checks browser support for event.
  if ('onhashchange' in window) {
    window.onhashchange = function (event) {
      var hashes = document.location.hash;
      console.log('Went to ' + hashes);
    };
  }

  setTimeout(function () {
    document.querySelector('.left-animation').style.width = '60%';
    document.querySelector('.right-animation').style.width = '60%';
  }, 5000);
  setInterval(carouselTick, 5000); //createAllArrows('.title');

  manageItemFocus('.focusable-list');
}

(function () {
  init();

  let wrapper=document.querySelector("#work-history");
  let temp="";
  let i=0;

  for(i=0;i<work_history.length;i++){
    temp="<o-card title='"+work_history[i].company+"' hoverable='true' block='1'>";
    temp+="<o-avatar slot='extra' size='20' src='"+work_history[i].icon+"'></o-avatar>";
    temp+="<o-card-body>";
    temp+="<div class='position-title'>"+work_history[i].position+"</div>";
    temp+="<div class='time-period'>"+work_history[i].start_date+ " - "+work_history[i].end_date+"</div>";
    temp+="<div class='company-name'>"+work_history[i].company+"</div>";
    temp+="</o-card-body></o-card>";

    wrapper.innerHTML+=temp;
  }

  wrapper=document.querySelector("#languages");
  for(i=0;i<languages.length;i++){
    temp="<o-card title='"+languages[i].name+"'"+"class='language-container expandable'>";
    temp+="<o-card-body id='"+languages[i].name+"' class='description'>"+languages[i].description+"</o-card-body></o-card>";
    wrapper.innerHTML+=temp;
  }

  wrapper=document.querySelector("#datastore");
  for(i=0;i<datastores.length;i++){
    temp="<o-card title='"+datastores[i].name+"'"+"class='datastore-container expandable'>";
    temp+="<o-card-body id='"+datastores[i].name+"' class='description'>"+datastores[i].description+"</o-card-body></o-card>";
    wrapper.innerHTML+=temp;
  }

  wrapper=document.querySelector("#frameworks");
  for(i=0;i<frameworks.length;i++){
    temp="<o-card title='"+frameworks[i].name+"'"+"class='framework-container expandable'>";
    temp+="<o-card-body id='"+frameworks[i].name+"' class='description'>"+frameworks[i].description+"</o-card-body></o-card>";
    wrapper.innerHTML+=temp;
  }

  console.log("Brand New Line");
})();
